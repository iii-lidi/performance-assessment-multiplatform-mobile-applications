-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require( "widget" )

local t = display.newText( "Waiting for button event...", 0, 0, native.systemFont, 18 )
t.x, t.y = display.contentCenterX, 70

local button1Press = function( event )

	initial = os.clock()

	serie = 0;

	for j=1,5 do 
		for k=1,100000 do
			serie = serie + ( math.log(k) / math.log(2) ) + (3*k/2*j) + math.sqrt(k) + math.pow(k, j-1);
		end
	end

	final = os.clock()
	tiempo = final - initial;	
	
	t.text = serie .. " -> " .. tiempo
end

local button1 = widget.newButton
{
	label = "Button 1 Label",
	emboss = true,
	onPress = button1Press,
}