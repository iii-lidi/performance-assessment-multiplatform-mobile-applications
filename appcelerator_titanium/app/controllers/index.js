function doClick(e) {
	
    var inicio = new Date().getTime();

    var serie = 0;

    for ( var j=1; j <= 5; j++ ){
        for ( var k=1; k <= 100000; k++ ){
            serie = serie + ( Math.log(k)/Math.LN2 ) + (3*k/2*j) + Math.sqrt(k) + Math.pow(k, j-1);
        }
    }

    var fin = new Date().getTime();
    var tiempo = fin - inicio;

    $.resultado.text = tiempo + ' -> ' + serie; 
}

$.index.open();
