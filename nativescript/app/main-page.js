"use strict";
var createViewModel = require("./main-view-model");
function onNavigatingTo(args) {
    var page = args.object;
    page.bindingContext = new createViewModel.HelloWorldModule();
}
exports.onNavigatingTo = onNavigatingTo;
//# sourceMappingURL=main-page.js.map