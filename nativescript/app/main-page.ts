import createViewModel = require("./main-view-model");
import {Page} from "ui/page";

export function onNavigatingTo(args) {
    var page = <Page>args.object;
    page.bindingContext = new createViewModel.HelloWorldModule();    
}